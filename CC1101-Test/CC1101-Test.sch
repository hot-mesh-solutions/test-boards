EESchema Schematic File Version 4
LIBS:CC1101-Test-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 5800 3200 850  650 
U 5BDC0033
F0 "Sheet5BDC0032" 50
F1 "CC1101.sch" 50
F2 "SCLK" B L 5800 3400 50 
F3 "SO" B L 5800 3500 50 
F4 "CSn" B L 5800 3800 50 
F5 "SI" B L 5800 3300 50 
F6 "GDO2" B L 5800 3600 50 
F7 "GDO0" B L 5800 3700 50 
$EndSheet
$Comp
L Connector:Conn_01x06_Male J1
U 1 1 5BDCFEF8
P 5100 3500
F 0 "J1" H 5206 3878 50  0000 C CNN
F 1 "Conn_01x06_Male" H 5206 3787 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 5100 3500 50  0001 C CNN
F 3 "~" H 5100 3500 50  0001 C CNN
	1    5100 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 3300 5300 3300
Wire Wire Line
	5800 3400 5300 3400
Wire Wire Line
	5800 3500 5300 3500
Wire Wire Line
	5800 3600 5300 3600
Wire Wire Line
	5800 3700 5300 3700
Wire Wire Line
	5800 3800 5300 3800
$EndSCHEMATC
